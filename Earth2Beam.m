function[b1 b2 b3 b4] = Earth2Beam(Ub,Vb,Wb,xt,yt,zt);
format long

hoek = atan2d(sqrt(xt.^2+yt.^2),zt);  %Angle with the vertical of each beam
azi = atan2d(yt,xt);                  %Angle with the x-axis for each beam
    
beamvel = (-Ub.*cosd(azi)-Vb.*sind(azi)).*sind(hoek)-Wb.*cosd(hoek);
  
b1 = squeeze(beamvel(1,:,:));
b2 = squeeze(beamvel(2,:,:));
b3 = squeeze(beamvel(3,:,:));
b4 = squeeze(beamvel(4,:,:));