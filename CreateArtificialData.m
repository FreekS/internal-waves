function [b1 b2 b3 b4 distance Uabove Vabove Wabove Upabove] = CreateArtificialData(xdim,ydim,zdim,tdim,rdim,xstep,ystep,zstep,tstep,rstep,Num,alpha,U0,theta,phi,pitch,roll,heading,speed)

format long

x = [-xdim:xstep:xdim]';  % Create grid in meters
y = [-ydim:ystep:ydim]';
z = [13:zstep:zdim]';  
t = [0:tstep:tdim]';
r = [0:rstep:rdim]';

xprime = x*cosd(alpha)+y*sind(alpha);  % Coordinate in wave direction

Lx = Num*2*zdim*tand(theta);  %wavelenth in wave direction
Lz = zdim;                    %water depth
kx = 2*pi/Lx;                 %wavenumber in wave direction
kz = pi/Lz;                   %depth wave number
omega = speed*kx;             %Frequency of the wave
distance = r*cosd(theta);     %Vertical distance from ADCP
Pitch = atand(tand(pitch).*cosd(roll)); %Ajustment for the pitch

[X,Y,Z,T] = ndgrid(x,y,z,t);

PhiZ = cos(kz*Z);   %shear function

Uprime = PhiZ.*U0.*cos(kx.*(X*cosd(alpha)+Y*sind(alpha))-omega.*T);  %Velocity in wave direction in earth coordinates
Vprime = PhiZ.*U0.*cos(kx.*(X*cosd(alpha)+Y*sind(alpha))-omega.*T)*0;
Wprime = kx/kz*U0*sin(kx.*(X*cosd(alpha)+Y*sind(alpha))-omega.*T).*sin(kz*Z);   

U = Uprime*cosd(alpha);        %Velocity in east,north,up in earth coordinates
V = Uprime*sind(alpha);       
W = Wprime;                    

[xt yt zt] = CoordinatesOTBeams(heading,Pitch,roll,t,r,theta,phi);  %Calculate the coordinates of the beams
time = repmat(t,1,size(r,1));

Ub = zeros(size(xt));
Vb = zeros(size(xt));
Wb = zeros(size(xt));
    
 for b = 1:4;
        Ub(b,:,:) = interpn(X,Y,Z,T,U,squeeze(xt(b,:,:)),squeeze(yt(b,:,:)),squeeze(zt(b,:,:)),time);     %U,V,W at the location of the beams in x,y,z,t
        Vb(b,:,:) = interpn(X,Y,Z,T,V,squeeze(xt(b,:,:)),squeeze(yt(b,:,:)),squeeze(zt(b,:,:)),time);
        Wb(b,:,:) = interpn(X,Y,Z,T,W,squeeze(xt(b,:,:)),squeeze(yt(b,:,:)),squeeze(zt(b,:,:)),time);
 end
    
Uabove = zeros(size(r,1),size(t,1));
Vabove = zeros(size(r,1),size(t,1));
Wabove = zeros(size(r,1),size(t,1));
Upabove = zeros(size(r,1),size(t,1));
 
 for rrr = 1:size(r,1);
        xa = r(rrr)*0;   %x,y,z coordinates along the beams
        ya = r(rrr)*0;
        za = r(rrr)*cosd(theta);
        
        Uabove(rrr,:) = interpn(X,Y,Z,T,U,xa,ya,za,t);     %U,V,W straight above the ADCP in x,y,z,t
        Vabove(rrr,:) = interpn(X,Y,Z,T,V,xa,ya,za,t);
        Wabove(rrr,:) = interpn(X,Y,Z,T,W,xa,ya,za,t);
        Upabove(rrr,:) = interpn(X,Y,Z,T,Uprime,xa,ya,za,t);
 end
    
Uabove = Uabove';  %flip matrix, calculation issue
Vabove = Vabove';
Wabove = Wabove';
Upabove = Upabove';

[b1 b2 b3 b4] = Earth2Beam(Ub,Vb,Wb,xt,yt,zt); %b1,b2,b3,b4 are the beamvelocities.

end