function [En Nn Un] = PhaseLag(window,Phi2,C2,t,r,distance,xt,yt,zt,theta,b1,b2,b3,b4,heading,Pitch,roll,up)

tdim = size(t,1);
rdim = size(r,1);
tstep = t(2)-t(1);
[ZZ,TT] = ndgrid(distance,t);

xt1 = squeeze(xt(1,:,:));  % Coordinates of the beams
yt1 = squeeze(yt(1,:,:));
zt1 = squeeze(zt(1,:,:));
xt2 = squeeze(xt(2,:,:));
yt2 = squeeze(yt(2,:,:));
zt2 = squeeze(zt(2,:,:));
xt3 = squeeze(xt(3,:,:));
yt3 = squeeze(yt(3,:,:));
zt3 = squeeze(zt(3,:,:));
xt4 = squeeze(xt(4,:,:));
yt4 = squeeze(yt(4,:,:));
zt4 = squeeze(zt(4,:,:));
zb = r*cosd(theta);
Xt1 = zeros(size(xt1));
Xt2 = zeros(size(xt2));
Xt3 = zeros(size(xt3));
Xt4 = zeros(size(xt4));
Yt1 = zeros(size(yt1));
Yt2 = zeros(size(yt2));
Yt3 = zeros(size(yt3));
Yt4 = zeros(size(yt4));

for i = 1:size(t,1);
Xt1(i,:) = interp1(zt1(i,:),xt1(i,:),zb, 'nearest','extrap');  % Coordinates of the BINMAPPED beams.
Yt1(i,:) = interp1(zt1(i,:),yt1(i,:),zb, 'nearest','extrap');
Xt2(i,:) = interp1(zt2(i,:),xt2(i,:),zb, 'nearest','extrap');
Yt2(i,:) = interp1(zt2(i,:),yt2(i,:),zb, 'nearest','extrap');
Xt3(i,:) = interp1(zt3(i,:),xt3(i,:),zb, 'nearest','extrap');
Yt3(i,:) = interp1(zt3(i,:),yt3(i,:),zb, 'nearest','extrap');
Xt4(i,:) = interp1(zt4(i,:),xt4(i,:),zb, 'nearest','extrap');
Yt4(i,:) = interp1(zt4(i,:),yt4(i,:),zb, 'nearest','extrap');
end

d1 = Xt1.*cosd(Phi2)+Yt1.*sind(Phi2);  % Distance from the beams to the origin in the wave direction
d2 = Xt2.*cosd(Phi2)+Yt2.*sind(Phi2);
d3 = Xt3.*cosd(Phi2)+Yt3.*sind(Phi2);  
d4 = Xt4.*cosd(Phi2)+Yt4.*sind(Phi2);

Time1 = zeros(tdim,rdim);
Time2 = zeros(tdim,rdim);
Time3 = zeros(tdim,rdim);
Time4 = zeros(tdim,rdim);
h = waitbar(0,'please wait')
for j = 1:size(distance,1);
    waitbar(j/size(distance,1),h)
            D1 = d1(:,j);  % Distance of the beams to the origin in wave direction for 1 depth for all time
            D2 = d2(:,j);
            D3 = d3(:,j);
            D4 = d4(:,j);  
                  h1 = waitbar(0,'TIme')
    for iii = (1+window):(size(t,1)-window);
        waitbar(iii/size(t,1),h1)
            Dc = C2*(t-((iii-1)*tstep)-t(1));        % Distance of the wave-front from the origin.
            [xi1,~] = intersections(t((iii-window):(iii+window)),D1((iii-window):(iii+window)),t((iii-window):(iii+window)),Dc((iii-window):(iii+window)));     % Comparing the coordinate in wave direction of the wave with the     
            [xi2,~] = intersections(t((iii-window):(iii+window)),D2((iii-window):(iii+window)),t((iii-window):(iii+window)),Dc((iii-window):(iii+window)));     % beams. This was the time at which the wave and beams intersect is calculated.
            [xi3,~] = intersections(t((iii-window):(iii+window)),D3((iii-window):(iii+window)),t((iii-window):(iii+window)),Dc((iii-window):(iii+window)));     
            [xi4,~] = intersections(t((iii-window):(iii+window)),D4((iii-window):(iii+window)),t((iii-window):(iii+window)),Dc((iii-window):(iii+window)));
            
            crit = isempty(xi1);
            if crit == 1;
                xi1 = nan;
            end
            Time1(iii,j) = xi1(1)-((iii-1)*tstep)-t(1);  
            % Only interestet in the time it took the wave to go from the
            % origin to the beam, not the actual time it happend (so not wave at origin = 100s wave at beam = 112s,
            % but time it took the wave to go from origin to beam is 112-100= 12s)
     
            crit = isempty(xi2);
            if crit == 1;
                xi2 = nan;
            end
            Time2(iii,j) = xi2(1)-((iii-1)*tstep)-t(1);
            
            crit = isempty(xi3);
            if crit == 1;
                xi3 = nan;
            end
            Time3(iii,j) = xi3(1)-((iii-1)*tstep)-t(1);
            
            crit = isempty(xi4);
            if crit == 1;
                xi4 = nan;
            end
            Time4(iii,j) = xi4(1)-((iii-1)*tstep)-t(1);
             
    end
end

n1 = interpn(TT',ZZ',b1,(TT'+Time1),ZZ');  %New phase-lagged beamvelocities
n2 = interpn(TT',ZZ',b2,(TT'+Time2),ZZ');  
n3 = interpn(TT',ZZ',b3,(TT'+Time3),ZZ');  
n4 = interpn(TT',ZZ',b4,(TT'+Time4),ZZ');  

[xn yn zn] = Beam2Inst(n1,n2,n3,n4,theta); %New phase-lageed instrument velocities

[En Nn Un] = Inst2Earth(heading,Pitch,roll,distance,xn,yn,zn,up); %New phase-lagged earth velocities


end