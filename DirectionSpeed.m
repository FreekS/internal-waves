function [C2 Phi2] = DirectionSpeed(t,r,distance,E,N,U,zhat)

tdim = size(t,1);
rdim = size(r,1);
tstep = t(2)-t(1);
[ZZ,TT] = ndgrid(distance,t);

duadt = NaN(tdim,rdim);
dwdz = NaN(tdim,rdim);

win = boxcar(4)/4;  %Filter the velocities a bit.

Efilt = filter(win, 1, E);
Nfilt = filter(win, 1, N);
Ufilt = filter(win, 1, U')';

phi2 = zeros(1,tdim);
                                % Deming regression to calculate the
                                % direction: phi = atan(v/u);
for i = 1:tdim;
         NoverE = deming(squeeze(Efilt(i,:))',squeeze(Nfilt(i,:))');  %Be sure that there is no bad data over the depth!
         phi2(i) = atand(NoverE(2));                                    % So remore the 'out of water'  data first!
end

Phi2 = nanmedian(phi2);   %Get 1 value for phi over the whole time.
ua = Efilt.*cosd(Phi2)+Nfilt.*sind(Phi2);  % Velocity in wave direction u'

dua = ua(3:end,:) - ua(1:end-2,:);
dt = t(3:end,:) - t(1:end-2,:);
duadt(2:end-1,:) = dua/dt(1); 

dw = Ufilt(:,3:end) - Ufilt(:,1:end-2);
dz = zhat(:,3:end) - zhat(:,1:end-2);
dwdz(:,2:end-1) = dw./dz;

C = zeros(1,tdim);
                        % Deming regression to calculate the speed: C =
                        % Time derivative of along wave speed divided by
                        % vertical derivative of vertical velocity.
                     
for ii = 1:tdim;
    UoverW = deming(squeeze(dwdz(ii,:))',squeeze(duadt(ii,:))');  %Again be sure that the data for the whole depth is good
    C(ii) = UoverW(2);
end

C2 = nanmedian(C); %Get 1 value of C for the whole time.
