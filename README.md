# README #


### What is this repository for? ###

This can be used to calculate the proper velocities in earth coordinates from an ADCPs beamvelocities using the phase-lag method

### How do I get set up? ###

There are 2 scripts with a lot of functions.

Beam2Inst2Eart is the main script.
It is used to load netcdf files, and calculate the velocities in earth coordinates as illustrated by the ADCP manuals.
Instead of using the netcdf files, one can also load in the artificial beamdata. An example of a testwave is given in TestArtificialData.
Finally the script also implements the phase-lag method.
Be aware that it was written to calculate the direction and speed of the NLIWs from the beamdata, but that does not work most of the time.
Therefor if you know the direction and speed of the wave, this can be put in manually.

CoordinatesOTBeams is used to calculate the time depentant x,y,z coordiante along the radial coordinate of the beams as function of time.
x,y,z are here earthcoordinates so x,y,z is pointing right,forward,up. It assumes the ADCP is pointing upward.

CreateArtificialData required a lot of input and an example is given by TestArtificialData. 
It returns the artifical velocity field in beamcoordinates as well as the velocities in earth coordinates straight above the ADCP.

DirectionSpeed calculates the direction and the speed using the velocities in earth coordintes. 
In my experience this is really troublesome when there is not a nice strong wave present.
It would be better to skip this step and get the speed and direction of the NLIW by different means.

PhaseLag is the functions it's all about. It uses the coordinates of the beams and binmaps these coordinates.
Than the coordinates are used to calculate the distance from the origin to the beam in the wave direction.
Then for each timestep, a wave is created and the time at which the wave intersects with the beams, minus the time the wave was at the origin is calculated.
This time is used to interpolate the beamvelocities on same phases of the wave.
The interpolated phase-lagged beamvelocities are than used to calculate the velocities in instrument and earth coordinates.
Because the intersection function take really long, a window can be set. The window must not be too large otherwise you lose data at the beginning and end.
And also not to small or the wave will not pass the beams in the time window. Window is set as the amount of indices in the time vector.

If the speed and direction were obtained with DirectionSpeed, you want to recalculate the direction and speed with the phase-lagged velocities.
Then redo the phase-lag with the new speed and direction. Keep repeating until speed and direction dont chang anymore.
If you already have to proper speed and direction by other means, than doing the PhaseLag only once is sufficient.



### What do I need? ###

Netcdf file of ADCP data.
Matlab 2015b or newer.

### Who do I talk to? ###

Freek Schellekens
Eindhoven University of Technology
f.j.schellekens@student.tue.nl
