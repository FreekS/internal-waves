function[xt yt zt] = CoordinatesOTBeams(heading,Pitch,roll,t,r,theta,phi)
% these are earth coordinates. so x,y,z are pointing right,forward and up.
% heading is compass angle, so negative coordinate angle.
% Assuming the ADCP points upwards. If ADCP points down 180 degrees could
% be added to the roll te fix this, however I am not sure about that since
% I havent looked into it myself.
format long

CH = cosd(heading);
CP = cosd(Pitch);
CR = cosd(roll);

SH = sind(heading);
SP = sind(Pitch);
SR = sind(roll);

dimd = size(r,1);
dimt = size(t,1);

CH = repmat(CH,1,dimd);
SH = repmat(SH,1,dimd);
CP = repmat(CP,1,dimd);
SP = repmat(SP,1,dimd);
CR = repmat(CR,1,dimd);
SR = repmat(SR,1,dimd);

M11 = (CH.*CR)+(SH.*SP.*SR);  %Transformation matrix
M12 = SH.*CP;
M13 = (CH.*SR)-(SH.*SP.*CR);

M21 = (-1.*SH.*CR)+(CH.*SP.*SR);
M22 = CH.*CP;
M23 = (-1.*SH.*SR)-(CH.*SP.*CR);

M31 = -CP.*SR;
M32 = SP;
M33 = CP.*CR;

xt = zeros(4,dimt,dimd);
yt = zeros(4,dimt,dimd);
zt = zeros(4,dimt,dimd);

for b = 1:4
xb = r*sind(theta)*cosd(phi(b));    %Coordinates of the beams if there was no heading,pitch,roll
yb = r*sind(theta)*sind(phi(b));
zb = r*cosd(theta);

xb = repmat(xb,1,dimt)';
yb = repmat(yb,1,dimt)';
zb = repmat(zb,1,dimt)';

xt(b,:,:) = M11.*xb + M12.*yb + M13.*zb;  %Coordinates of the beams as function of time incl heading pitch roll
yt(b,:,:) = M21.*xb + M22.*yb + M23.*zb;
zt(b,:,:) = M31.*xb + M32.*yb + M33.*zb;
end
