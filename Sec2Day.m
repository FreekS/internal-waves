function[time_days] = Sec2Day(startdate,time);
startd = strrep(startdate,'seconds since 2','2');             %remove the text 'seconds since' 
std = strrep(startd,'0.000003','0');                          %remove the 3 microseconds
datum = datetime(std,'InputFormat','yyyy-MM-dd HH:mm:ss');    %read the start date and time
time_days = datum+time/86400;                                 %make array of date and time since start, 86400 is sec in 1 day