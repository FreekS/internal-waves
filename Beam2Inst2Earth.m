%% Load the files
% 1 = WP 150 kHz  high up
% 2 = NP 300 kHz  low up
% 3 = WP 300 kHz  low down
% 4 = SP 75 kHz   low up
% 5 = BRB 150 kHz low up
% 6 = NP 150 kHz  high up

take = 4;

if take == 1;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_150_kHz_Quartermaster_11795/time','units');
    figuretitle = 'WP250 RDI 150 kHz Quartermaster 11795';
    up = 1; %1 for up, -1 for down
    
elseif take == 2;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_300_kHz_Monitor_20089/time','units');
    figuretitle = 'NP250 RDI 300 kHz Monitor 20089';
    up = 1; %1 for up, -1 for down
   
elseif take == 3;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/WP250_RDI_300_kHz_Sentinel_20092/time','units');
    figuretitle = 'WP250 RDI 300 kHz Sentinel 20092';
    up = -1; %1 for up, -1 for down
         
elseif take == 4;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/SP250_RDI75_kHz_Longranger_24613/time','units');
    figuretitle = 'SP250 RDI75 kHz Longranger 24613';
    up = 1; %1 for up, -1 for down
  
elseif take == 5;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/BRB200_RDI_150_kHz_Quartermaster_16752/time','units');
    figuretitle = 'BRB200 RDI 150 kHz Quartermaster 16752';
    up = 1; %1 for up, -1 for down
   
elseif take == 6;
    pressure = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/pressure');
    temperature = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/temperature');
    time = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/time');
    heading = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/heading');
    pitch = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/pitch');
    roll = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/roll');
    echo = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/echo');
    distance = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/distance');
    beamvel = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/beamvel');
    u = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/u');
    v = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/v');
    w = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/w');
    zhat = ncread('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/zhat');
    startdate = ncreadatt('KISSME2017_RDI_QAQC_float64.NC','/NP250_RDI_150_kHz_Quartermaster_16856/time','units');
    figuretitle = 'NP250 RDI 150 kHz Quartermaster 16856';
    up = 1; %1 for up, -1 for down
   
end    
    
%% convert time in seconds to actual dates since started     
time_days = Sec2Day(startdate,time);
t = time;

%% Get beamvel from each beam
b1 = squeeze(beamvel(:,1,:));    %split the data for different beams
b2 = squeeze(beamvel(:,2,:));
b3 = squeeze(beamvel(:,3,:));
b4 = squeeze(beamvel(:,4,:));

%%% From this step you can also put in the artificial beamvelocities to
%%% transform those to velocities in Earth Coordinates

%% Beam 2 instrument
Pitch = atand(tand(pitch).*cosd(roll));  %Adjustment for the pitch due to ADCP measurement
theta = 20; %angle between beams and vertical in degrees
phi = [0 180 90 270]; %angle for each beam with the x-axis
r = distance/cosd(theta);  %Create the postion vector of the beams from the distance which is vertical from ADCP

[xt yt zt] = CoordinatesOTBeams(heading,Pitch,roll,t,r,theta,phi);  %Get coordinates of the beams

[b1 b2 b3 b4] = BinMap(b1,b2,b3,b4,zt,t,r,theta);   %Binmap the vertical coordinates of the beams on the Bins

[x y z] = Beam2Inst(b1,b2,b3,b4,theta);

%% Instrument 2 earth

[E N U] = Inst2Earth(heading,Pitch,roll,distance,x,y,z,up);

%% Phase-lag
[C2 Phi2] = DirectionSpeed(t,r,distance,E,N,U,zhat);

C2 = 1;
Phi2 = -40;
window = 10; 

[En Nn Un] = PhaseLag(window,Phi2,C2,t,r,distance,xt,yt,zt,theta,b1,b2,b3,b4,heading,Pitch,roll,up);
%%% PhaseLag takes really long for big time vectors. I suggest not using
%%% more than 1000 steps in time.
%%% A time window is created to make calculations faster.
%%% Make it too big and you lose data at the edges, to small and you lose
%%% data in between if the actual time is out of the window.
%%% Use window to set a window in which the wave is expacted to pass the
%%% beams. window is in time vector indices. So if your time vector has
%%% steps of 60 seconds, and the beamlength is 300 meters and the wavespeed
%%% is 1 m/s, than taking a window of 10 is nice, since it will take 600
%%% seconds, which is more than enough.