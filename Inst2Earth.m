function[E N U] = Inst2Earth(heading,Pitch,roll,distance,x,y,z,up)
format long
if up == 1;      % due to the x and z pointing left and down, they need to be flipped to creaete earth velocities East and Up.
roll = roll+180;
end

CH = cosd(heading);
CP = cosd(Pitch);
CR = cosd(roll);

SH = sind(heading);
SP = sind(Pitch);
SR = sind(roll);

dimd = size(distance,1);

CH = repmat(CH,1,dimd);
SH = repmat(SH,1,dimd);
CP = repmat(CP,1,dimd);
SP = repmat(SP,1,dimd);
CR = repmat(CR,1,dimd);
SR = repmat(SR,1,dimd);

M11 = (CH.*CR)+(SH.*SP.*SR);   % rotation matrix
M12 = SH.*CP; 
M13 = (CH.*SR)-(SH.*SP.*CR);

M21 = (-1.*SH.*CR)+(CH.*SP.*SR);
M22 = CH.*CP;
M23 = (-1.*SH.*SR)-(CH.*SP.*CR);

M31 = -CP.*SR;
M32 = SP;
M33 = CP.*CR;

E = M11.*x + M12.*y + M13.*z;
N = M21.*x + M22.*y + M23.*z;
U = M31.*x + M32.*y + M33.*z;

