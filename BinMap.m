function[B1 B2 B3 B4] = BinMap(b1,b2,b3,b4,zt,t,r,theta)

zb = r*cosd(theta);  %Vertical distance from the ADCP of each bin

zt1 = squeeze(zt(1,:,:));  %Vertical distance from ADCP of each beam
zt2 = squeeze(zt(2,:,:));
zt3 = squeeze(zt(3,:,:));
zt4 = squeeze(zt(4,:,:));

B1 = zeros(size(b1));
B2 = zeros(size(b2));
B3 = zeros(size(b3));
B4 = zeros(size(b4));

for i = 1:size(t,1);

B1(i,:) = interp1(zt1(i,:),b1(i,:),zb, 'nearest','extrap'); %Map the vertical coordinates of each beam on the coordinates of each Bin
B2(i,:) = interp1(zt2(i,:),b2(i,:),zb, 'nearest','extrap');
B3(i,:) = interp1(zt3(i,:),b3(i,:),zb, 'nearest','extrap');
B4(i,:) = interp1(zt4(i,:),b4(i,:),zb, 'nearest','extrap');
end
