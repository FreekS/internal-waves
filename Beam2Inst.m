function[x y z] = Beam2Inst(b1,b2,b3,b4,theta);
format long
x = (b1-b2)/(2*sind(theta));  % x relative west
y = (b4-b3)/(2*sind(theta));  % y relative north
z = (b1+b2+b3+b4)/(4*cosd(theta));  % z relative down